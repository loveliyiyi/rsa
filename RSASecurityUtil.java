import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @ClassName: RSACoder
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author 1332727844@qq.com
 * @date 2017年7月20日 下午4:53:56
 * 
 */
public class RSASecurityUtil {
	private static final String KEY_ALGORITHM = "RSA";
	private static final int KEY_SIZE = 512;
	private static final int ENCRYPT_BlOCK_SIZE = KEY_SIZE / 8 - 11;
	private static final int DECRYPT_BLOCK_SIZE = KEY_SIZE / 8;

	public static KeyPair getKeyPair() throws Exception {
		KeyPairGenerator kpGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		kpGenerator.initialize(KEY_SIZE);
		KeyPair keyPair = kpGenerator.generateKeyPair();
		return keyPair;
	}

	/**
	 * 加密
	 * 
	 * @param encryptStr
	 * @param publicKey
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String encryptStr, String publicKey) throws Exception {
		byte[] publicKeyBytes = Base64.getDecoder().decode(publicKey);
		byte[] encryptBytes = encryptStr.getBytes("UTF-8");

		if (encryptBytes.length <= ENCRYPT_BlOCK_SIZE) {
			return Base64.getEncoder().encodeToString(encrypt(encryptBytes, publicKeyBytes));
		} else {
			byte[] buffer = null;
			byte[] blockBytes = new byte[ENCRYPT_BlOCK_SIZE];

			int index = ((encryptBytes.length - 1) / ENCRYPT_BlOCK_SIZE) + 1;

			for (int i = 0; i < index; i++) {
				if (i == (index - 1)) {
					blockBytes = new byte[ENCRYPT_BlOCK_SIZE];
				}
				int startIndex = i * ENCRYPT_BlOCK_SIZE;
				int endIndex = startIndex + ENCRYPT_BlOCK_SIZE;
				blockBytes = ArrayUtils.subarray(encryptBytes, startIndex, endIndex);
				if (buffer == null) {
					buffer = encrypt(blockBytes, publicKeyBytes);
				} else {
					buffer = ArrayUtils.addAll(buffer, encrypt(blockBytes, publicKeyBytes));
				}

			}
			return Base64.getEncoder().encodeToString(buffer);
		}
	}

	/**
	 * 解密
	 * 
	 * @param decryptStr
	 * @param privateKey
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String decryptStr, String privateKey) throws Exception {
		byte[] privateKeyBytes = Base64.getDecoder().decode(privateKey);

		byte[] decryptBytes = Base64.getDecoder().decode(decryptStr);

		if (decryptBytes.length <= DECRYPT_BLOCK_SIZE) {
			return new String(decrypt(decryptBytes, privateKeyBytes), "UTF-8");
		} else {
			byte[] buffer = null;

			int index = ((decryptBytes.length - 1) / DECRYPT_BLOCK_SIZE) + 1;
			byte[] blockBytes = new byte[DECRYPT_BLOCK_SIZE];
			for (int i = 0; i < index; i++) {
				if (i == index - 1) {
					blockBytes = new byte[DECRYPT_BLOCK_SIZE];
				}
				int startIndex = i * DECRYPT_BLOCK_SIZE;
				int endIndex = startIndex + DECRYPT_BLOCK_SIZE;
				blockBytes = ArrayUtils.subarray(decryptBytes, startIndex,
						endIndex > decryptBytes.length ? decryptBytes.length : endIndex);
				if (buffer == null) {
					buffer = decrypt(blockBytes, privateKeyBytes);
				} else {
					buffer = ArrayUtils.addAll(buffer, decrypt(blockBytes, privateKeyBytes));
				}
			}
			return new String(buffer, "UTF-8");
		}
	}

	/**
	 * 加密
	 * 
	 * @param encryptStr
	 * @param publicKeyBytes
	 * @return
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] encryptBytes, byte[] publicKeyBytes) throws Exception {
		PublicKey publicKey = RSASecurityUtil.codeToPublicKey(publicKeyBytes);

		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] enBytes = cipher.doFinal(encryptBytes);
		return enBytes;
	}

	/**
	 * 解密
	 * 
	 * @param decryptStr
	 * @param privateKeyBytes
	 * @return
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] decrypt, byte[] privateKeyBytes) throws Exception {
		PrivateKey privateKey = RSASecurityUtil.codeToPrivateKey(privateKeyBytes);

		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] resultBytes = cipher.doFinal(decrypt);
		return resultBytes;
	}

	/**
	 * 解密
	 * 
	 * @param dncrypteStr
	 * @param privateKeyBytes
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String decryptStr, byte[] privateKeyBytes) throws Exception {
		PrivateKey privateKey = RSASecurityUtil.codeToPrivateKey(privateKeyBytes);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] decryptBytes = Base64.getDecoder().decode(decryptStr);
		byte[] resultBytes = cipher.doFinal(decryptBytes);
		return new String(resultBytes, "UTF-8");
	}

	public static PrivateKey codeToPrivateKey(String privateKeyStr) throws Exception {
		byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyStr);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		PrivateKey keyPrivate = keyFactory.generatePrivate(keySpec);
		return keyPrivate;
	}

	public static PublicKey codeToPublicKey(byte[] publicKey) throws Exception {
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKey);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		return keyFactory.generatePublic(keySpec);
	}

	public static PrivateKey codeToPrivateKey(byte[] privateKey) throws Exception {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKey);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		PrivateKey keyPrivate = keyFactory.generatePrivate(keySpec);
		return keyPrivate;
	}

	public static void main(String[] args) throws Exception {
		KeyPair keyPair = RSASecurityUtil.getKeyPair();
		String pub = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
		String pri = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());
		System.out.println("公钥：" + pub);
		System.out.println("私钥：" + pri);

		String string = "ssssssssssss";
		String jiami = RSASecurityUtil.encrypt(string, pub);
		System.out.println("加密串:" + jiami);
		String jiemi = RSASecurityUtil.decrypt(jiami, pri);
		System.out.println("解密串:" + jiemi);
	}
}
